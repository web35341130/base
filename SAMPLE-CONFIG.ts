import * as dotenv from "dotenv";
import path from "path";
import { fileURLToPath } from "url";
import { Modules } from "./src/types.js";

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

dotenv.config({ path: path.resolve(__dirname, "PATH TO YOUR WALLET FILE") });

if (!process.env.WALLETS) throw new Error("Missing wallet data");

export const config = {
	EXCLUDED_WALLETS: [] as number[],
	SECRET_WALLET_DATA: JSON.parse(process.env.WALLETS),
	MODULES: {
		PREVENT_SENDING_MAX_ETHER: true, // prevents from swapping entire eth balance if forget to provide amount in settings
		MINMAX_WALLET_WAIT_TIME: [60 * 3, 60 * 7], // seconds
		MINMAX_MODULES_WAIT_TIME: [60 * 3, 60 * 7], // seconds
		PROXY_ENFORCE: true, // true/false
		PROXY_RPC_REQUESTS: false,
		ORDER: "",
		MAX_ETH_GWEI: 18, // number or null for current gwei
		[Modules.BRIDGE]: {
			ENABLED: true, // true/false
			MINMAX_DEPOSIT_AMOUNT: [0.001, 0.002] as [number, number] | [], // empty for max balance
		},
		[Modules.BUILDERS_NFT]: {
			ENABLED: true, // true/false
		},
	},
};
