// ALL CHAINS
export const Chains = {
	ETH: "ethereum",
	BASE: "base",
} as const;
export type ChainsType = (typeof Chains)[keyof typeof Chains];
export type ChainsTypeKey = keyof typeof Chains;
export type ChainsData = {
	rpc: string;
	explorer: string;
	chainId: number;
};
export type ChainsDataType = Record<ChainsType, ChainsData>;

// TOKENS
export const Tokens = {
	USDC: "usdc",
	USDT: "usdt",
	ETH: "eth",
	WBTC: "wbtc",
} as const;
export type TokensType = (typeof Tokens)[keyof typeof Tokens];
export type TokensTypeKey = keyof typeof Tokens;
export type TokensData = {
	address: string;
	symbol: string;
};
export type TokensDataType = Record<TokensType, TokensData>;

// ORDER
export const ORDER: Record<string, OrderType> = {
	RANDOM: "random",
	ONE_RANDOM: "one_random",
	DEFAULT: "default",
} as const;
export type OrderType = "random" | "one_random" | "default";

// PROXY
export type Proxy = {
	ip: string;
	port: string;
	username: string;
	password: string;
};

export type Proxies = {
	[name: string]: Proxy;
};

// MISC
// export type WalletData = {
// 	name: number;
// 	address: string;
// 	signer: Wallet | HDNodeWallet;
// };

export type WalletData = {
	name: string;
	privateKey: string;
};

// MODULES
export const MODULES = {
	BRIDGE: "BRIDGE",
	BUILDERS_NFT: "BUILDERS_NFT",
	BRIDGE_TO_BASE_NFT: "BRIDGE_TO_BASE_NFT",
} as const;
export type ModulesType = (typeof MODULES)[keyof typeof MODULES];
