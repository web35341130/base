import { Contract } from "ethers";
import { logger } from "../logger.js";
import { estimateGas } from "./estimateGas.js";
import { getTransactionState } from "./getTransactionState.js";

export async function sendTransaction(
	contract: Contract,
	methodName: string,
	message: string,
	value: bigint = BigInt(0),
	...txArgs: any[]
) {
	const gas = await estimateGas(contract, methodName, ...txArgs, {
		value,
	});

	logger.info`### Sending transaction ###`;
	const swapTx = await contract[methodName](...txArgs, {
		...gas,
		value,
	});

	const receipt = await getTransactionState(swapTx, message);

	return receipt;
}
