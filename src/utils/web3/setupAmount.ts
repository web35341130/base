import { BigNumberish, Contract, ethers } from "ethers";
import { setTimeout } from "timers/promises";
import { config } from "../../../config.js";
import { ProviderManager } from "../../../data/chain-data.js";
import { TokenManager } from "../../../data/token-data.js";
import { WalletManager } from "../../../data/wallet-data.js";
import { Chains, ChainsType, Tokens, TokensType } from "../../types.js";
import { logger } from "../logger.js";
import { randomFloat } from "../utils.js";
import { getTransactionState } from "./getTransactionState.js";

export async function setupAmount(
	token: TokensType,
	values: [number, number] | [],
	spender?: string,
	chain: ChainsType = Chains.BASE
): Promise<BigNumberish> {
	const provider =
		chain === Chains.BASE
			? ProviderManager.baseProvider
			: ProviderManager.ethProvider;

	let amountToSend;
	let amount;
	if (values.length === 2) amount = randomFloat(values[0], values[1], 4);

	if (token === Tokens.ETH) {
		if (amount) {
			amountToSend = ethers.parseEther(amount.toString());
		} else if (!config.MODULES.PREVENT_SENDING_MAX_ETHER) {
			logger.error`ABOUT TO SEND ENTIRE ETH BALANCE. CONSIDER CANCELLING...`;
			await setTimeout(10000);
			const balance = await provider.getBalance(WalletManager.address);
			const buffer = (balance * BigInt(2)) / BigInt(100);
			amountToSend = balance - buffer;
		} else {
			throw new Error("No valid ETH amount");
		}
	} else {
		const tokenContract = TokenManager.getContract(token).connect(
			WalletManager.baseWallet
		) as Contract;
		const tokenAddress = TokenManager.getAddress(token);
		const decimals = await tokenContract.decimals();

		const balance = await tokenContract.balanceOf(WalletManager.address);

		if (amount) {
			amountToSend = ethers.parseUnits(amount.toString(), decimals);
			if (amountToSend > balance)
				throw new Error("Token amount is larger than balance");
		} else {
			amountToSend = balance;
		}

		const allowance = await tokenContract.allowance(spender, tokenAddress);
		if (amountToSend.gt(allowance)) {
			const approveTx = await tokenContract.approve(
				spender,
				amountToSend.toString()
			);
			await getTransactionState(
				approveTx,
				`approving transaction for token ${token}`
			);
		}
	}

	return amountToSend;
}
