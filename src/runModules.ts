import { config } from "../config.js";
import { runBaseBuildersNFT } from "./modules/baseBuildersNFTManager.js";
import { runBridgeManager } from "./modules/bridgeManager.js";
import { runBridgeToBaseNFT } from "./modules/bridgeToBaseNFTManager.js";
import { MODULES, ModulesType, ORDER } from "./types.js";
import { countdownTimer } from "./utils/countdownTimer.js";
import { logger } from "./utils/logger.js";
import { shuffleArr } from "./utils/utils.js";
import { waitForETHGas } from "./utils/web3/waitForETHGas.js";

type Module = {
	fn: Function;
	args: any[];
	name: ModulesType;
};

export async function runModules() {
	const modules: Module[] = [
		{
			fn: runBridgeManager,
			args: [],
			name: MODULES.BRIDGE,
		},
		{
			fn: runBaseBuildersNFT,
			args: [],
			name: MODULES.BUILDERS_NFT,
		},
		{
			fn: runBridgeToBaseNFT,
			args: [],
			name: MODULES.BRIDGE_TO_BASE_NFT,
		},
	];

	let enabledModules = modules.filter(
		(module) => config.MODULES[module.name].ENABLED
	);

	switch (config.MODULES.ORDER) {
		case ORDER.RANDOM:
			shuffleArr(enabledModules);
			break;

		case ORDER.ONE_RANDOM:
			if (config.MODULES.BRIDGE.ENABLED)
				enabledModules = enabledModules.splice(
					enabledModules.findIndex(
						(module) => module.name === MODULES.BRIDGE
					),
					1
				);
			enabledModules = shuffleArr(enabledModules);
			enabledModules.splice(1);
			break;
	}

	for (let i = 0; i < enabledModules.length; i++) {
		const module = enabledModules[i];
		const moduleName = module.name;

		try {
			if (config.MODULES.MAX_ETH_GWEI) await waitForETHGas();
			logger.addCustomPrepend(`[${moduleName}]`);
			await module.fn(...module.args);
			logger.success`Module ${moduleName} executed successfully.`;
			if (enabledModules[i + 1])
				await countdownTimer(
					config.MODULES.MINMAX_MODULES_WAIT_TIME[0],
					config.MODULES.MINMAX_MODULES_WAIT_TIME[1]
				);
		} catch (error) {
			logger.error`Module ${moduleName} failed, switching to next one...`;
			console.log(error);
		}
	}
}
