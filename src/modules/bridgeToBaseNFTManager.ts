import { ethers } from "ethers";
import { WalletManager } from "../../data/wallet-data.js";
import { Chains } from "../types.js";
import { asyncRetry } from "../utils/asyncRetry.js";
import { logger } from "../utils/logger.js";
import { handleModuleError } from "../utils/web3/handleModuleError.js";
import { sendTransaction } from "../utils/web3/sendTransaction.js";

async function mintBridgeToBase() {
	const operation = async () => {
		logger.info`### Staring mint BRIDGETOBASE NFT ###`;

		const abi = [
			"function claim(address _receiver, uint256 _quantity, address _currency, uint256 _pricePerToken, tuple(bytes32[] proof, uint256 quantityLimitPerWallet, uint256 pricePerToken, address currency) _allowlistProof, bytes _data) payable",
		];

		const mintContract = new ethers.Contract(
			"0xea2a41c02fa86a4901826615f9796e603c6a4491",
			abi,
			WalletManager.baseWallet
		);

		const txParams = {
			receiver: WalletManager.address,
			quantity: 1,
			currency: "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE",
			pricePerToken: 0,
			allowlistProof: {
				proof: [],
				quantityLimitPerWallet: ethers.MaxUint256,
				pricePerToken: 0,
				currency: "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE",
			},

			bytes: ethers.hexlify("0x"),
		};

		const txArgs = Object.values(txParams);

		await sendTransaction(
			mintContract,
			"claim",
			`mint BRIDGETOBASE NFT`,
			undefined,
			...txArgs
		);
	};

	await asyncRetry(operation, handleModuleError, [
		"claim",
		WalletManager.address,
		Chains.BASE,
	]);
}

export const runBridgeToBaseNFT = async () => {
	await mintBridgeToBase();
};
