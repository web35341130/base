import { ethers } from "ethers";
import { Got } from "got";
import { BASE_BUILDERS_NFT_ABI } from "../../data/abis/base-builders-nft-abi.js";
import { WalletManager } from "../../data/wallet-data.js";
import { Chains } from "../types.js";
import { asyncRetry } from "../utils/asyncRetry.js";
import { GotManager } from "../utils/gotManager.js";
import { logger } from "../utils/logger.js";
import { handleModuleError } from "../utils/web3/handleModuleError.js";
import { sendTransaction } from "../utils/web3/sendTransaction.js";

type VerifyMessageResponse = {
	d: {
		__type: string;
		success: boolean;
		verifyResult: string;
		saveResult: string;
		isDuplicatedMessage: boolean;
		verifiedMessageLocation: string;
	};
};

export class BaseBuildersNFTManager {
	#got: Got;

	constructor() {
		this.#got = GotManager.got.extend({
			headers: {
				authority: "basescan.org",
				origin: "https://basescan.org",
				referer: "https://basescan.org/verifiedSignatures",
			},
		});
	}

	async #verifyMessageSignature() {
		logger.info`Generating and verifying signature...`;

		await this.#got.get("https://basescan.org/verifiedSignatures#");

		const messageRaw = "all your base are belong to you.";
		const messageSignature = await WalletManager.baseWallet.signMessage(
			messageRaw
		);

		const payload = {
			address: WalletManager.address,
			messageRaw,
			messageSignature,
			saveOption: "1",
		};

		const response: VerifyMessageResponse = await this.#got
			.post(
				"https://basescan.org/verifiedSignatures.aspx/VerifyMessageSignature",
				{
					json: payload,
				}
			)
			.json();

		if (response.d.success) {
			return messageSignature;
		}
		logger.error`Auth request error: ${response}`;
	}

	public async mint() {
		const operation = async () => {
			logger.info`### Staring mint BUILDERS NFT... ###`;

			const signature = await this.#verifyMessageSignature();

			const mintContract = new ethers.Contract(
				"0x1FC10ef15E041C5D3C54042e52EB0C54CB9b710c",
				BASE_BUILDERS_NFT_ABI,
				WalletManager.baseWallet
			);

			await sendTransaction(
				mintContract,
				"mint",
				`minting BUILDERS NFT`,
				undefined,
				signature
			);
		};

		await asyncRetry(operation, handleModuleError, [
			"mint",
			WalletManager.address,
			Chains.BASE,
		]);
	}
}

export const runBaseBuildersNFT = async () => {
	const baseBuilders = new BaseBuildersNFTManager();
	await baseBuilders.mint();
};
