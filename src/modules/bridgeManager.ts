import { Contract, ethers } from "ethers";
import { config } from "../../config.js";
import { WalletManager } from "../../data/wallet-data.js";
import { Chains, Tokens } from "../types.js";
import { asyncRetry } from "../utils/asyncRetry.js";
import { logger } from "../utils/logger.js";
import { getTransactionState } from "../utils/web3/getTransactionState.js";
import { handleModuleError } from "../utils/web3/handleModuleError.js";
import { pollBalance } from "../utils/web3/pollBalance.js";
import { setupAmount } from "../utils/web3/setupAmount.js";

async function bridgeETH() {
	const walletAddress = WalletManager.address;
	const operation = async () => {
		logger.info`### Starting bridge ###`;

		const abi = [
			"function depositTransaction(address _to, uint256 _value, uint64 _gasLimit, bool _isCreation, bytes _data) payable",
		];

		const contract = new Contract(
			"0x49048044D57e1C92A77f79988d21Fa8fAF74E97e",
			abi,
			WalletManager.ethWallet
		);

		const values = config.MODULES.BRIDGE.MINMAX_DEPOSIT_AMOUNT;

		const value = await setupAmount(
			Tokens.ETH,
			values,
			undefined,
			Chains.ETH
		);

		const txParams = {
			to: WalletManager.address,
			value,
			gasLimit: 100000,
			isCreation: false,
			data: ethers.hexlify("0x01"),
		};

		const txArgs = Object.values(txParams);

		const bridgeTx = await contract.depositTransaction(...txArgs, {
			value,
			gasLimit: 200000,
		});

		await getTransactionState(
			bridgeTx,
			`bridging ${ethers.formatEther(value)} ETH ==> BASE`
		);

		await pollBalance({ network: Chains.BASE, walletAddress });
	};

	await asyncRetry(operation, handleModuleError, [
		"bridge",
		walletAddress,
		Chains.ETH,
	]);
}

export const runBridgeManager = async () => {
	await bridgeETH();
};

// ARCHIVED - OLD BRIDGE
// async function bridgeETH() {
// 	const walletAddress = WalletManager.address;
// 	const operation = async () => {
// 		logger.info`Starting bridge...`;

// 		const values = config.MODULES.BRIDGE.MINMAX_DEPOSIT_AMOUNT;

// 		const value = await setupAmount(
// 			Tokens.ETH,
// 			values,
// 			undefined,
// 			Chains.ETH
// 		);

// 		const bridgeTx = await WalletManager.ethWallet.sendTransaction({
// 			to: "0x49048044D57e1C92A77f79988d21Fa8fAF74E97e",
// 			value,
// 			gasLimit: randomNumber(60000, 80000),
// 		});

// 		await getTransactionState(
// 			bridgeTx,
// 			`bridging ${ethers.formatEther(value)} ETH to BASE`
// 		);

// 		await pollBalance({ network: Chains.BASE, walletAddress });
// 	};

// 	await asyncRetry(operation, handleModuleError, [
// 		"bridge",
// 		walletAddress,
// 		Chains.ETH,
// 	]);
// }
