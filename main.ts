import { config } from "./config.js";
import { ProviderManager } from "./data/chain-data.js";
import { WalletManager } from "./data/wallet-data.js";
import { runModules } from "./src/runModules.js";
import { WalletData } from "./src/types.js";
import { countdownTimer } from "./src/utils/countdownTimer.js";
import { errorHandler } from "./src/utils/errorHandler.js";
import { GotManager } from "./src/utils/gotManager.js";
import { importProxies } from "./src/utils/importProxies.js";
import { logger } from "./src/utils/logger.js";
import { shuffleArr } from "./src/utils/utils.js";

const { EXCLUDED_WALLETS } = config;
const excludedWalletsSet = new Set(EXCLUDED_WALLETS);

const proxies = await importProxies("./proxies.txt");

process.on("unhandledRejection", (reason, promise) => {
	if (reason instanceof Error) {
		errorHandler(reason);
	} else {
		console.error("Unhandled Rejection at:", promise, "reason:", reason);
	}
});

async function processWallet(walletData: WalletData) {
	const { name, privateKey } = walletData;

	WalletManager.init(privateKey);
	const address = WalletManager.address;

	logger.setCustomPrepend(`[Name: ${name}][${address}]`);

	const proxy = proxies[name];

	await GotManager.initialize(proxy);
	ProviderManager.setProxy();

	await runModules();

	logger.success`Task completed, waiting for next wallet...`;
}

async function processWallets(wallets: WalletData[]) {
	for (let i = 0; i < wallets.length; i++) {
		const wallet = wallets[i];
		if (excludedWalletsSet.has(+wallet.name)) {
			logger.info`Skipping wallet ${wallet.name} as it's in the excluded list.`;
			continue;
		}
		await processWallet(wallet);
		if (wallets[i + 1])
			await countdownTimer(
				config.MODULES.MINMAX_WALLET_WAIT_TIME[0],
				config.MODULES.MINMAX_WALLET_WAIT_TIME[1]
			);
	}
	logger.success`Automation job completed`;
}

async function main() {
	const wallets: WalletData[] = shuffleArr(config.SECRET_WALLET_DATA ?? []);

	if (wallets.length === 0) throw new Error("Wallets array is empty");

	// const filWallets = wallets.filter((wallet) => +wallet.name === 7);
	await processWallets(wallets);

	process.exit(0);
}

main();
