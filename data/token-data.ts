import { Contract, getAddress } from "ethers";
import { Tokens, TokensDataType, TokensType } from "../src/types.js";
import { ERC_20_ABI } from "./abis/erc-20-abi.js";

export class TokenManager {
	static #tokens: TokensDataType = {
		[Tokens.USDC]: {
			address: "",
			symbol: "USDC",
		},
		[Tokens.USDT]: {
			address: "",
			symbol: "USDT",
		},
		[Tokens.ETH]: {
			address: "",
			symbol: "ETH",
		},
		[Tokens.WBTC]: {
			address: "",
			symbol: "WBTC",
		},
	};

	static getContract(token: TokensType): Contract {
		return new Contract(
			getAddress(this.#tokens[token].address),
			ERC_20_ABI
		);
	}

	static getAddress(token: TokensType): string {
		return getAddress(this.#tokens[token].address);
	}
}
