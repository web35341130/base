import { Wallet } from "ethers";
import { ProviderManager } from "./chain-data.js";

export class WalletManager {
	static #ethWallet: Wallet | null = null;
	static #baseWallet: Wallet | null = null;
	static #address: string | null = null;

	static get ethWallet(): Wallet {
		if (!this.#ethWallet) throw new Error("Eth wallet not initialized");
		return this.#ethWallet;
	}

	static get baseWallet(): Wallet {
		if (!this.#baseWallet) throw new Error("Base wallet not initialized");
		return this.#baseWallet;
	}

	static get address(): string {
		if (!this.#address) throw new Error("Wallet not initialized");
		return this.#address;
	}

	public static init(privateKey: string | Wallet): void {
		const ethProvider = ProviderManager.ethProvider;
		const baseProvider = ProviderManager.baseProvider;
		if (typeof privateKey === "string") {
			this.#ethWallet = new Wallet(privateKey, ethProvider);
			this.#baseWallet = new Wallet(privateKey, baseProvider);
		} else {
			this.#ethWallet = privateKey.connect(ethProvider);
			this.#baseWallet = privateKey.connect(baseProvider);
		}
		this.#address = this.#ethWallet.address;
	}
}
