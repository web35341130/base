import { FetchRequest, JsonRpcProvider } from "ethers";
import { config } from "../config.js";
import { Chains, ChainsDataType, ChainsType } from "../src/types.js";
import { GotManager } from "../src/utils/gotManager.js";
import { logger } from "../src/utils/logger.js";

export class ProviderManager {
	static #providers: ChainsDataType = {
		[Chains.ETH]: {
			rpc: "https://eth.llamarpc.com",
			explorer: "https://etherscan.io",
			chainId: 1,
		},
		[Chains.BASE]: {
			rpc: "https://mainnet.base.org",
			explorer: "https://basescan.org/",
			chainId: 8453,
		},
	};

	static #createProvider(rpc: string): JsonRpcProvider {
		return new JsonRpcProvider(rpc);
	}

	public static get ethProvider(): JsonRpcProvider {
		return this.#createProvider(this.#providers[Chains.ETH].rpc);
	}

	public static get baseProvider(): JsonRpcProvider {
		return this.#createProvider(this.#providers[Chains.BASE].rpc);
	}

	public static getProvider(chain: ChainsType): JsonRpcProvider {
		return this.#createProvider(this.#providers[chain].rpc);
	}

	static getExplorer(chain: ChainsType): string {
		return this.#providers[chain].explorer;
	}

	static getExplorerByChainId(chainId: number): string {
		for (let chain in this.#providers) {
			if (this.#providers[chain as ChainsType].chainId === chainId) {
				return this.#providers[chain as ChainsType].explorer;
			}
		}
		throw new Error("Wrong chain Id, no provider found");
	}

	static setProxy(): void {
		if (config.MODULES.PROXY_RPC_REQUESTS) {
			logger.warn`RPC requests will be sent over a proxy...`;
			const customFetchFunc = async (req: any, signal: any) => {
				try {
					const options: any = {
						method: req.method,
						headers: req.headers,
					};

					if (req.body) {
						options.body = Buffer.from(req.body);
					}

					const gotResponse = await GotManager.got(req.url, options);

					const getUrlResponse = {
						body: gotResponse.body
							? Uint8Array.from(Buffer.from(gotResponse.body))
							: null,
						headers: gotResponse.headers,
						statusCode: gotResponse.statusCode,
						statusMessage: gotResponse.statusMessage,
					};

					return getUrlResponse;
				} catch (err) {
					console.log(err);
				}
			};
			FetchRequest.registerGetUrl(customFetchFunc as any);
		} else {
			logger.warn`RPC requests are sent without a proxy...`;
		}
	}
}
